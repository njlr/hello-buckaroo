# hello-buckaroo

This project is for integration tests of [Buckaroo](https://buckaroo.pm) with GitLab.

## Building the Library 📚

```
buck build :hello-buckaroo
```

## Installing the Package 🏗️

```
buckaroo install gitlab+njlr/hello-buckaroo
```

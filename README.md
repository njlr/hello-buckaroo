# hello-buckaroo

This project is for integration tests of [Buckaroo](https://buckaroo.pm) with BitBucket. 

## Building the Library 📚

```
buck build :hello-buckaroo
```

## Installing the Package 🏗️

```
buckaroo install bitbucket+njlr/hello-buckaroo
```
